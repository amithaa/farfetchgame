﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour {

	public string id;

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(StartGameNow);
	}

	void StartGameNow()
	{
		RandomTileGenerator.Instance.folderName = id;

		MainManager.Instance.OpenGamePanel ();
		MainManager.Instance.StartGame ();
	}


}
