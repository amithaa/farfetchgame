﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VendingScript : MonoBehaviour
{


	private static VendingScript _instance;

	public static VendingScript Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	public GameObject qrScanPanel;
	public GameObject pinPanel;
	public GameObject vendingPanel;

	public InputField pinInput;

	public int pin;


	public void OpenQRPanel ()
	{
		qrScanPanel.SetActive (true);
		pinPanel.SetActive (false);
		vendingPanel.SetActive (false);
	}


	public void OpenPinPanel ()
	{
		qrScanPanel.SetActive (false);
		pinPanel.SetActive (true);
		vendingPanel.SetActive (false);
	}


	public void OpenVendingPanel ()
	{
		qrScanPanel.SetActive (false);
		pinPanel.SetActive (false);
		vendingPanel.SetActive (true);

		DisableVending ();
	}


	/// <summary>
	/// Activate QR field
	/// </summary>
	public void ActivateQRField ()
	{
		pinInput.Select ();
		pinInput.ActivateInputField ();
	}


	public void Submit()
	{
		//Add db connection here
		ReadSerialInput.Instance.SerialCmdSend(pin.ToString());
		OpenVendingPanel ();
	}



	public void ResetAll ()
	{
		QRCodeManagerOld.Instance.ResetAll ();
		pinInput.text = "";
	}


	public void DisableVending ()
	{
		StartCoroutine (DisableVendingPanel ());

	}


	public void SavePin (string txt)
	{
		try {
			pin = int.Parse (txt);
		} catch (System.Exception e) {
			QRCodeManagerOld.Instance.AlertError ("Enter valid pin");
		}
	}

	IEnumerator DisableVendingPanel ()
	{

		yield return new WaitForSeconds (10.0f);

		ResetAll ();

		OpenQRPanel ();

	}
}
