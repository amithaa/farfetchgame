﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnderTileScript : MonoBehaviour {

	public int id;
	public string filename;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void FlipUpperTile()
	{
		MainManager.Instance.listOfAllTiles [id].GetComponent<Animator> ().SetBool ("flipback",true);
	}
}
