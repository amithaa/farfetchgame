﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class MainManager : MonoBehaviour
{

	private static MainManager _instance;

	public static MainManager Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	public List<GameObject> listOfAllTiles = new List<GameObject> ();
	public List<GameObject> listOfAllUnderTiles = new List<GameObject> ();

	[SerializeField]
	public static int clickCount;
	public int gameCount = 0;

	Stack<GameObject> memoryStack = new Stack<GameObject> ();

	public ParticleSystem confetti1;
	public ParticleSystem confetti2;

	public bool winner = false;

	public GameObject mainPanel;
	public GameObject messagePanel;
	public Text message;

	public GameObject choicePanel;
	public GameObject menWomenPanel;

	public GameObject menPanel;
	public GameObject womenPanel;


	public Text score;

	public ToggleGroup parentToggle;


	public List<GameObject> listOfQRCodes = new List<GameObject> ();

	public GameObject qrCodePrefab;
	public GameObject qrParent;

	public AudioSource winning;

	public int clickedTileID;


	public Image qrcodeimg;

	void Start ()
	{
		clickCount = 0;

	}



	public void CheckClickCount (GameObject clickedBtn)
	{
		clickCount++;
//		listOfFlippedTiles.Add (clickedBtn);
		memoryStack.Push (clickedBtn);
		if (clickCount == 2) {
			//Check for the id
			GameObject firstElement = memoryStack.Pop ();
			GameObject secondElement = memoryStack.Pop ();

			int index1 = firstElement.GetComponent<TileScript> ().id;
			int index2 = secondElement.GetComponent<TileScript> ().id;

			if (listOfAllUnderTiles [index1].GetComponent<UnderTileScript> ().id == listOfAllUnderTiles [index2].GetComponent<UnderTileScript> ().id) {
				gameCount++;

				score.text = (gameCount * 100).ToString ();

				GetComponent<AudioSource> ().Play ();

				if (gameCount == RandomTileGenerator.Instance.tilePairs.Count) {
					print ("Game over");
					winner = true;
					winning.Play ();
					TimerScript.Instance.FinishTimer (gameCount);

					try {
						string returnstring = DatabaseConnection.Instance.SubmitForDraw (QRCodeManagerOld.Instance.qrcodeString);
					} catch (System.Exception e) {
						QRCodeManagerOld.Instance.AlertError ("There was a problem connecting to DB");
					}
				}

				clickCount = 0;


			} else {
				StartCoroutine (DelayToDisplay (firstElement, secondElement));

			}
		}
	}


	public void DisplayQRCodes ()
	{

//		for(int i = 0 ; i < RandomTileGenerator.Instance.listOfSelectedImages.Count ; ++i) {
//		GameObject qrImage = Instantiate (qrCodePrefab, qrParent.transform);

		string filename = "QRCodes/" + RandomTileGenerator.Instance.folderName;

		print (filename);
//
		qrcodeimg.sprite = Resources.Load<Sprite> (filename);
//		qrImage.GetComponentInChildren<Text> ().text = RandomTileGenerator.Instance.folderName;
//
//		listOfQRCodes.Add (qrImage);

//		}
	}

	public void OpenMenWomenPanel ()
	{
		mainPanel.SetActive (false);
		choicePanel.SetActive (false);
		menWomenPanel.SetActive (true);
	}


	public void OpenChoicePanel ()
	{
		mainPanel.SetActive (false);
		choicePanel.SetActive (true);
		menWomenPanel.SetActive (false);

	}

	public void OpenGamePanel ()
	{
		mainPanel.SetActive (false);
		choicePanel.SetActive (false);
		menWomenPanel.SetActive (false);
	}


	public void SetMenOrWoman (int i)
	{
		if (i == 0) {
			menPanel.SetActive (true);
			womenPanel.SetActive (false);
		} else {
			womenPanel.SetActive (true);
			menPanel.SetActive (false);
		}

		OpenChoicePanel ();
	}


	//	public void GetActiveToggle ()
	//	{
	//		Toggle active = parentToggle.ActiveToggles ().FirstOrDefault ();
	//		RandomTileGenerator.Instance.folderName = active.GetComponent<ToggleScript> ().id;
	//
	//		OpenGamePanel ();
	//		StartGame ();
	//	}


	IEnumerator DelayToDisplay (GameObject firstElement, GameObject secondElement)
	{
		yield return new WaitForSeconds (1.0f);

		int flipID01 = firstElement.GetComponent<TileScript> ().id;
		int flipID02 = secondElement.GetComponent<TileScript> ().id;

		MainManager.Instance.listOfAllUnderTiles [flipID01].GetComponent<Animator> ().SetBool ("flipback", false);
		MainManager.Instance.listOfAllUnderTiles [flipID02].GetComponent<Animator> ().SetBool ("flipback", false);

		yield return new WaitForSeconds (0.3f);

		firstElement.GetComponent<Animator> ().SetBool ("flip", false);
		secondElement.GetComponent<Animator> ().SetBool ("flip", false);

	}

	public void StartGame ()
	{

		RandomTileGenerator.Instance.StartOver ();
		TimerScript.Instance.StartTimer ();
		MainManager.Instance.mainPanel.SetActive (false);
		RevealAll ();

	}


	public void RevealAll ()
	{
		if (listOfAllTiles.Count == listOfAllUnderTiles.Count) {
			for (int i = 0; i < listOfAllTiles.Count; ++i) {
				listOfAllTiles [i].GetComponent<Animator> ().SetTrigger ("Reveal");
				listOfAllUnderTiles [i].GetComponent<Animator> ().SetTrigger ("Reveal");

			}
		}
	}


	public void ResetGame ()
	{
		clickCount = 0;
		listOfAllTiles.Clear ();
		listOfAllUnderTiles.Clear ();
		listOfQRCodes.Clear ();

		memoryStack.Clear ();

		RandomTileGenerator.Instance.ResetAll ();

		mainPanel.SetActive (true);
		messagePanel.SetActive (false);

		winner = false;
		gameCount = 0;

		menPanel.SetActive (true);
		womenPanel.SetActive (true);

		message.text = "";
		score.text = "0";
		TimerScript.Instance.timerText.text = "";

		foreach (Transform child in qrParent.transform) {
			Destroy (child.gameObject);
		}


		QRCodeManagerOld.Instance.ActivateQRField ();

	}



	public void DisplayMessageForWinOrLose ()
	{
		StartCoroutine (DisplayMessage ());

	}


	IEnumerator DisplayMessage ()
	{
		yield return new WaitForSeconds (1.0f);

		messagePanel.SetActive (true);

		if (winner) {
			confetti1.Play ();
			confetti2.Play ();
		} 

		yield return new WaitForSeconds (10.0f);

		ResetGame ();

	}
		

}
