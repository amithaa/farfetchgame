﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System;
using System.Text;

public class DatabaseConnection : MonoBehaviour
{
	private static DatabaseConnection _instance;

	public static DatabaseConnection Instance { get { return _instance; } }


	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public string ipaddress = "http://195.229.43.5/gitex/api/";

	string username = "P1xel@NikeDxb";
	string password = "#N3wy@rk!!";

	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public string FindUserIdentityWithQRCode (string _qrcode)
	{
		try {
			print(_qrcode);
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("https://pixelplusmedia.com/farfetch/api/v3.0/qrcode");
			httpWebRequest.ContentType = "application/json";
//			httpWebRequest.Headers.Add ("Authorization:token f34e190a0fad8882e727dcf1c0da922b");
			httpWebRequest.Method = "POST";
			string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
			httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"passid\":\"" + _qrcode + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				print (data);
				return data;
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			return string.Empty;
		}
	}



	public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
		bool isOk = true;
		// If there are errors in the certificate chain, look at each error to determine the cause.
		if (sslPolicyErrors != SslPolicyErrors.None) {
			for(int i=0; i<chain.ChainStatus.Length; i++) {
				if(chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown) {
					chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
					chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
					chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
					chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
					bool chainIsValid = chain.Build((X509Certificate2)certificate);
					if(!chainIsValid) {
						isOk = false;
					}
				}
			}
		}
		return isOk;
	}



	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public string SubmitForDraw (string _qrcode)
	{
		try {
			print(_qrcode);
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("https://pixelplusmedia.com/farfetch/api/v3.0/draw");
			httpWebRequest.ContentType = "application/json";
//			httpWebRequest.Headers.Add ("Authorization:token f34e190a0fad8882e727dcf1c0da922b");
			httpWebRequest.Method = "POST";
			string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
			httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"passid\":\"" + _qrcode + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				print (data);
				return data;
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			return string.Empty;
		}
	}




	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public string SendToVendingMachine (int value)
	{
		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("https://pixelplusmedia.com/farfetch/api/v2.0/draw");
			httpWebRequest.ContentType = "application/json";
			//			httpWebRequest.Headers.Add ("Authorization:token f34e190a0fad8882e727dcf1c0da922b");
			httpWebRequest.Method = "POST";
			string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
			httpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
			ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"passid\":\"" + value + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				print (data);
				return data;
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			return string.Empty;
		}
	}

}
