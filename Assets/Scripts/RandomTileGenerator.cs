﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class RandomTileGenerator : MonoBehaviour
{

	private static RandomTileGenerator _instance;

	public static RandomTileGenerator Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	public int rows;
	public int columns;

	//	int firstVal, secondVal, thirdVal, fourthVal, fifthVal;
	public List<int> listOfRandomNumbers = new List<int> ();
	public Dictionary<int,int> tilePairs = new Dictionary<int,int> ();
	public List<int> listOfIds = new List<int> ();

	public List<Sprite> listOfImages = new List<Sprite> ();

	public List<string> listOfSelectedImages = new List<string>();

	public GameObject parentPanel;
	public GameObject tilePrefab;

	public GameObject underParentPanel;
	public GameObject underTilePrefab;


	public int totalNumber;


	public string folderName = "MensBags";


	void Start ()
	{
		totalNumber = rows * columns;


//		StartOver ();
	}


	void CreateTiles ()
	{
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				GameObject tile = Instantiate (tilePrefab, parentPanel.transform);
				tile.GetComponent<TileScript> ().id = columns * i + j;
				MainManager.Instance.listOfAllTiles.Add (tile);
			}
		}

		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				GameObject undertile = Instantiate (underTilePrefab, underParentPanel.transform);
				MainManager.Instance.listOfAllUnderTiles.Add (undertile);
			}
		}

		try {
			listOfImages = Resources.LoadAll<Sprite> (folderName).ToList ();
		} catch (System.Exception e) {
			print (e.ToString ());
		}

	}


	void CreateUnderTiles ()
	{

		foreach (GameObject ob in MainManager.Instance.listOfAllTiles) {
			listOfIds.Add (ob.GetComponent<TileScript> ().id);
		}

		RandomGenerator.Shuffle (listOfIds);
		RandomGenerator.Shuffle (listOfImages);
		int count = 0;

		for (int i = 0; i < listOfIds.Count; i = i + 2) {
			tilePairs.Add (listOfIds [i], listOfIds [i + 1]);
//			print (listOfIds [i] + "........." + listOfIds [i + 1]);
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i]].GetComponent<UnderTileScript> ().id = count;
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i]].GetComponent<Image> ().sprite = listOfImages [count];
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i]].GetComponent<UnderTileScript> ().filename = listOfImages [count].name;
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i]].GetComponentInChildren<Text> ().text = count.ToString ();
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i + 1]].GetComponent<UnderTileScript> ().id = count;
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i + 1]].GetComponentInChildren<Text> ().text = count.ToString ();
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i + 1]].GetComponent<UnderTileScript> ().filename = listOfImages [count].name;
			MainManager.Instance.listOfAllUnderTiles [listOfIds [i + 1]].GetComponent<Image> ().sprite = listOfImages [count];
			count++;

			listOfSelectedImages.Add (listOfImages [count].name);
//			listOfSelectedImages = listOfSelectedImages.Distinct ().ToList ();
		}
			
	}


	public void ResetAll ()
	{
		listOfIds.Clear ();
		tilePairs.Clear ();
		listOfSelectedImages.Clear ();

		foreach (Transform child in parentPanel.transform) {
			Destroy (child.gameObject);
		}

		foreach (Transform child in underParentPanel.transform) {
			Destroy (child.gameObject);
		}
	}


	public void StartOver ()
	{
		CreateTiles ();
		CreateUnderTiles ();

	}

		
}
