﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
	private static TimerScript _instance;

	public static TimerScript Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public Text timerText;
	private float startTime;
	bool timerFinished = true;

	public float timeLimit;


	public void StartTimer ()
	{
		timerFinished = false;
		startTime = Time.time;

	}

	public void FinishTimer (int gamecount)
	{
		timerFinished = true;
		timerText.text = "";
		MainManager.Instance.message.text = gamecount.ToString ();
		MainManager.Instance.DisplayQRCodes ();

		MainManager.Instance.DisplayMessageForWinOrLose ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (timerFinished)
			return;
		
		float t = Time.time - startTime;
		if (t > timeLimit)
			FinishTimer (MainManager.Instance.gameCount);
		else {
			string seconds = (Mathf.Ceil(timeLimit - t)).ToString ();
			timerText.text = seconds;
		}
	}
}
