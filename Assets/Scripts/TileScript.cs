﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileScript : MonoBehaviour {

	public int id;
	bool flipped = false;

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(FlipTile);
	}


	public void FlipTile()
	{
		print (MainManager.clickCount);
		if (MainManager.clickCount < 2) {

			gameObject.GetComponent<Animator> ().SetBool ("flip", true);
			flipped = true;
			MainManager.Instance.clickedTileID = id;

			MainManager.Instance.CheckClickCount (gameObject);
		}

	}


	public void UnFlipTile()
	{
		gameObject.GetComponent<Animator> ().SetBool ("flip", false);
		flipped = false;
	}


	public void FlipUnderTile()
	{
		MainManager.Instance.listOfAllUnderTiles [id].GetComponent<Animator> ().SetBool ("flipback",true);
	}


	public void ResetCount()
	{
		MainManager.clickCount = 0;
	}
}
