﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class JSONParser
{

	/// <summary>
	/// Parse and Return User id
	/// </summary>
	/// <param name="_jsonstring">The string message</param>
	/// <param name="userid">User ID</param>
	public void CheckValidity (string _jsonString, out string valid, out string username)
	{
		valid = "";
		username = "";
		var N = JSON.Parse (_jsonString);
		valid = N ["result"];
		username = N ["name"];

	}
		

}
