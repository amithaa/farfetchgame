﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Threading;
using System;
using System.Text;

public class ReadSerialInput : MonoBehaviour
{

	private static ReadSerialInput _instance;

	public static ReadSerialInput Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	private Thread thread;

	private Queue inputQueue;

	SerialPort stream;

	bool runThread = true;


	void Start ()
	{
		try {
			inputQueue = Queue.Synchronized (new Queue ());
			stream = new SerialPort ("\\\\.\\COM8", 9600);
			thread = new Thread (ThreadLoop);
			thread.Start ();
			stream.ReadTimeout = 50;

			stream.Handshake = System.IO.Ports.Handshake.None;

			stream.Parity = Parity.None;


			stream.DataBits = 8;

			stream.StopBits = StopBits.One;

			stream.ReadTimeout = 200;

			stream.WriteTimeout = 50;
			stream.Open ();
		} catch (System.Exception e) {
			Debug.Log ("System is not setup" + e.ToString ());
		}
	}



	public void SerialCmdSend (string data)
	{

		if (stream.IsOpen) {
			try {
				byte[] hexstring = Encoding.ASCII.GetBytes (data);
				foreach (byte hexval in hexstring) {
					print("Here");
					byte[] _hexval = new byte[] { hexval }; // need to convert byte to byte[] to write
					stream.Write (_hexval, 0, 1);
					stream.BaseStream.Flush();
					Thread.Sleep (100);
				}

			} catch (Exception ex) {
				print ("I can’t seem to find serial ports.\n Please restart or contact support.");
			}

		}

	}




	void Update ()
	{
		CheckQueue ();

	}

	
	void CheckQueue ()
	{
		if (inputQueue.Count != 0) {
			int receivedMessage = (int)inputQueue.Dequeue ();

		}
	}

	public void ThreadLoop ()
	{
		// Opens the connection on the serial port
//
//		stream.ReadTimeout = 50;
//		stream.Open ();

		while (runThread) {
			if (stream.IsOpen) {
				try {
					int receivedMessage = stream.ReadByte ();
					print (receivedMessage);


				} catch (System.Exception) {

				}
			}
		}

	}


	void OnApplicationQuit ()
	{
		stream.Close ();
		runThread = false;
		thread.Join (500);
	}

}
