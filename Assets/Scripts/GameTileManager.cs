﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTileManager : MonoBehaviour
{

	public void FlipButton (GameObject tile)
	{
		tile.GetComponent<Animator> ().SetBool ("flip", true);
		MainManager.Instance.CheckClickCount (tile);
	}

}
