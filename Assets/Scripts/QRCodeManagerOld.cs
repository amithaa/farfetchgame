﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class QRCodeManagerOld : MonoBehaviour
{

	private static QRCodeManagerOld _instance;

	public static QRCodeManagerOld Instance { get { return _instance; } }


	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	//QR code input
	public InputField qrCodeInput;

	//Final QRCode string
	public string qrcodeString;

	public bool activateSending = false;

	public Text debugText;

	JSONParser jp = new JSONParser ();

	public bool waitingForQR = false;

	public GameObject pleaseWaitPanel;

	public Text nameField;


	void Start ()
	{
		ActivateQRField ();
	}


	/// <summary>
	/// Activate QR field
	/// </summary>
	public void ActivateQRField ()
	{
		qrCodeInput.Select ();
		qrCodeInput.ActivateInputField ();
		waitingForQR = true;

	}

	void Update ()
	{
		if (waitingForQR) {
			if (qrCodeInput.isFocused == false) {
				EventSystem.current.SetSelectedGameObject (qrCodeInput.gameObject, null);
				qrCodeInput.OnPointerClick (new PointerEventData (EventSystem.current));
			}
		}
	}


	/// <summary>
	/// Read input
	/// </summary>
	public void ReadQRCodeInput (string inputText)
	{
		waitingForQR = false;
		qrcodeString = inputText;
		//print(qrcodeString);

		if (!activateSending) {
			activateSending = true;
			pleaseWaitPanel.SetActive (true);
			StartCoroutine ("SendForVerification");
		}
			
	}



	public void ReadQRCodeInputForVending (string inputText)
	{
		waitingForQR = false;

		qrcodeString = inputText;
		//print(qrcodeString);

		if (!activateSending) {
			activateSending = true;
			pleaseWaitPanel.SetActive (true);
			StartCoroutine ("SendForVerificationForVending");
		}

	}



	public void ReadQRCodeInputForQR (string inputText)
	{
		waitingForQR = false;

		qrcodeString = inputText;
		//print(qrcodeString);

		if (!activateSending) {
			activateSending = true;
			pleaseWaitPanel.SetActive (true);
			StartCoroutine ("SendForVerificationQR");
		}

	}



	IEnumerator SendForVerification ()
	{
		yield return new WaitForSeconds (1f);
		CheckUserIdentity ();
	}


	IEnumerator SendForVerificationForVending ()
	{
		yield return new WaitForSeconds (1f);
		CheckUserIdentityForVending ();
	}


	IEnumerator SendForVerificationQR ()
	{
		yield return new WaitForSeconds (1f);
		CheckUserIdentityForQR ();
	}
		


	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public void CheckUserIdentity ()
	{
		print (qrcodeString);
		string valid = "";
		string username = "";
		try {
			string returnString = DatabaseConnection.Instance.FindUserIdentityWithQRCode (qrcodeString);
			jp.CheckValidity (returnString, out valid, out username);
			print(valid);

			nameField.text = username + "!";
			pleaseWaitPanel.SetActive (false);

			if (string.Equals (valid, "valid")) {
				//Validate
//				AlertError ("QR code string exists");

				MainManager.Instance.OpenMenWomenPanel ();

			} else {
				ResetAll();
				AlertError ("Invalid Pass");

			}

		} catch (System.Exception e) {
			
			AlertError ("Invalid Pass");
			ResetAll ();
			pleaseWaitPanel.SetActive (false);

		}

		activateSending = false;
	}



	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public void CheckUserIdentityForQR ()
	{
		print (qrcodeString);
		string valid = "";
		string username = "";
		try {
			string returnString = DatabaseConnection.Instance.FindUserIdentityWithQRCode (qrcodeString);
			jp.CheckValidity (returnString, out valid, out username);
			pleaseWaitPanel.SetActive (false);
			nameField.text = username + "!";

			if (string.Equals (valid, "valid")) {
				//Validate
				AlertError ("You have been entered for the draw");

				ResetAll ();
			} else {
				ResetAll();
				AlertError ("Invalid Pass");

			}

		} catch (System.Exception e) {

			AlertError ("Invalid Pass");
			ActivateQRField ();
			pleaseWaitPanel.SetActive (false);

		}

		activateSending = false;
	}




	/// <summary>
	/// Pay using QR Code
	/// </summary>
	public void CheckUserIdentityForVending ()
	{
		print (qrcodeString);
		string valid = "";
		string username = "";

		try {
			string returnString = DatabaseConnection.Instance.FindUserIdentityWithQRCode (qrcodeString);
			jp.CheckValidity (returnString, out valid, out username);
			pleaseWaitPanel.SetActive (false);
			nameField.text = username + "!";

			if (string.Equals (valid, "valid")) {
//				AlertError ("QR code string exists");

				VendingScript.Instance.OpenPinPanel ();
				VendingScript.Instance.ActivateQRField ();

				//Validate
			} else {
				ResetAll();
				AlertError ("Invalid Pass");

			}

		} catch (System.Exception e) {

			AlertError ("Invalid Pass");
			ResetAll ();
			pleaseWaitPanel.SetActive (false);

		}

		activateSending = false;
	}


	public void ResetAll ()
	{
		qrCodeInput.text = "";
		qrcodeString = "";
		activateSending = false;
		ActivateQRField ();
		pleaseWaitPanel.SetActive (false);

	}


	/// <summary>
	/// Display error message
	/// </summary>
	public void AlertError (string errorText)
	{
		debugText.gameObject.SetActive (true);
		debugText.text = errorText;
		StartCoroutine (DisableErrorText ());
	}


	/// <summary>
	/// Disable error message
	/// </summary>
	IEnumerator DisableErrorText ()
	{
		yield return new WaitForSeconds (5.0f);
		debugText.text = "";
		debugText.gameObject.SetActive (false);

	}



}
